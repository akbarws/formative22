import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import Todo from './components/todo/ToDo';

function App() {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}

export default App;
