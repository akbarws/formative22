import ToDoAdd from "./ToDoAdd";
import ToDoModal from "./ToDoModal";
import ToDoView from "./ToDoView";

import { useState } from "react";

import "./ToDo.css";
// import ToDoView2 from "./ToDoView2";

const Todo = () => {
  const [data, setData] = useState([]);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [message, setMessage] = useState("");

  function addData(newData) {
    setData((state) => {
      return [...state, newData];
    });
  }

  return (
    <div className="todo">
      <ToDoAdd
        addData={addData}
        handleShow={handleShow}
        setModalMessage={setMessage}
      />
      <ToDoView data={data} />
      {/* <ToDoView2 data={data} /> */}
      <ToDoModal show={show} handleClose={handleClose} message={message} />
    </div>
  );
};

export default Todo;
