import "./ToDoAdd.css";
import { useState } from "react";
import ButtonToDo from "./ButtonToDo";

const ToDoAdd = (props) => {
  const [newData, setNewData] = useState({
    username: "",
    age: 0,
  });

  function inputHandler(key, value) {
    setNewData((state) => {
      return { ...state, [key]: value };
    });
  }

  function isEmpty(val) {
    return val === "";
  }

  function save() {
    if (isEmpty(newData.username)) {
      props.handleShow();
      props.setModalMessage("Username ga boleh kosong");
      return;
    }
    if (isEmpty(newData.age)) {
      props.handleShow();
      props.setModalMessage("Age ga boleh kosong");
      return;
    }
    if(newData.age<1){
      props.handleShow();
      props.setModalMessage("Age > 0");
      return;
    }
    props.addData(newData);
  }

  return (
    <div className="todoadd">
      <h4 className="mb-3">Username</h4>
      <input
        type="text"
        name="username"
        className="mb-3"
        onChange={(e) => inputHandler(e.target.name, e.target.value)}
      />
      <h4 className="mb-3">Age (Years)</h4>
      <input
        type="number"
        name="age"
        className="mb-3"
        onChange={(e) => inputHandler(e.target.name, e.target.value)}
      />
      <br />
      <ButtonToDo onclick={save} />
    </div>
  );
};

export default ToDoAdd;
