import "./ToDoView.css";
import { Table } from "react-bootstrap";

const ToDoView = (props) => {
  return (
    <div className="todoview">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Username</th>
            <th>Age</th>
          </tr>
        </thead>
        <tbody>
          {props.data.length > 0 ? (
            props.data.map((item) => {
              return (
                <tr>
                  <td>{item.username}</td>
                  <td>{item.age}</td>
                </tr>
              );
            })
          ) : (
            <tr>
              <td colSpan="2">No Data</td>
            </tr>
          )}
        </tbody>
      </Table>
    </div>
  );
};

export default ToDoView;
