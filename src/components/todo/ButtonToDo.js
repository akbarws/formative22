import { Button } from "react-bootstrap";

const ButtonToDo = (props) => {
  return (
    <Button variant="success" onClick={props.onclick}>
      Submit
    </Button>
  );
};

export default ButtonToDo;
